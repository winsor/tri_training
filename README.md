# tri_training
This git explores the paper "Tri-Training: Exploiting Unlabeled Data Using Three Classifiers" by Zhou and Li (2005).

The git incldes:
A 20 minute video includes detailed code walk-through and analysis of results
A set of PowerPoint slides accompanying the video

Tri-training is a technique to train supervised classifiers using unlabeled data.  This is important because unlabeled data is often abundant, but the supervised learning techniques need labeled data.  Tri-Training allows using unlabeled data to train supervised models such as CNN, Bayes, and decision trees.

Implementation Notes:
git repository on bitbucket (https://bitbucket.org/winsor/tri_training)
video produced using Shotcut, MS-Voice Recorder for audio, slide screencapture via MS-Paint
slides are .pptx by MS-Powerpoint and LibreOffice
video stored on vimeo
website host is GoDaddy
